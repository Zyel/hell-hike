﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public Animator anim;
    [HideInInspector]
    public Rigidbody2D body;
    public Transform gunPivot;
    public Transform visual;
    public SpriteRenderer gunVisual;

    public Projectile projectile;
    public LineRenderer line;
    public ParticleSystem WallSlideParticle;
    public ParticleSystem DeadParticle;

    private float axisX;
    private float axisY;

    public float shotForce;

    public float wallSlideSpeed;
    public float wallSpeed;
    public float walkSpeed;        
    public float jumpForce;    
    public float lineRaycast;
    public float aimTimeScale;
    public float speedDash;
    public float timeAccelartionDash;

    public bool inputJumpDown;
    public bool inputJumpUp;
    public bool inputRight;
    public bool inputLeft;
    public bool aimInput;
    public bool cutJump;

    public bool onRoof;
    public bool onGround;
    public bool onRightWall;
    public bool onLeftWall;
    public bool onRoofWall;
    public bool onHeadWall;

    public bool hasGun;

    public AnimationCurve accelerationCurve;
    [Space(5)]
    public AnimationCurve accelerationAir;

    public AnimationCurve jumpCurve;

    Tweener dashTween;

    public float cursor;

    public float speedAcceleration;
    public float speedAirAcceleration;

    public PlayerState currentState;

    public AnimatorStateInfo walkStateInfo;

    Tweener timeScaleTween;

    private Camera cam;

    private Vector3 shotDirection;

    private Vector2 curVel;

    void Start()
    {
        cam = Camera.main;
        body = GetComponent<Rigidbody2D>();
        ChangeState(currentState);
        walkStateInfo = anim.GetCurrentAnimatorStateInfo(1);

        projectile.transform.parent = null;
    }

    void Update()
    {
        Checkground();
        CheckWall();

        switch (currentState)
        {
            case PlayerState.Idle:          IdleState   (); break;
            case PlayerState.Walk:          WalkState   (); break;
            case PlayerState.Jump:          AirState    (); break;
            case PlayerState.Aim:           AimState    (); break;
            case PlayerState.Shot:          ShotState   (); break;
            case PlayerState.WallClimb:     WallState   (); break;
            case PlayerState.Dash:          DashState   (); break;
        }        
    }

    private Vector2 dashDirection;
    private Tweener rotationDashTween;

    public void ChangeState(PlayerState newState)
    {
        switch (currentState)
        {
            case PlayerState.Idle:
                anim.SetBool("Idle", false);
                break;
            case PlayerState.Walk:
                anim.SetBool("Walking", false);
                anim.SetBool("GroundSlide", false);
                anim.speed = 1;
                break;
            case PlayerState.Jump:
                break;cutJump = false;
                anim.SetFloat("Air", 0);
            case PlayerState.Aim:
                gunPivot.gameObject.SetActive(false);
                timeScaleTween?.Kill();

                if(newState != PlayerState.Shot)
                {
                    timeScaleTween = DOTween.To(() => Time.timeScale, x => Time.timeScale = x, 1f, aimTimeScale * 0.5f).SetUpdate(true).OnUpdate(() =>
                    {
                        Time.fixedDeltaTime = Time.timeScale * 0.02f;
                    });
                }
                else
                {
                    Time.timeScale = 1;
                    Time.fixedDeltaTime = Time.timeScale * 0.02f;
                }
                
                break;
            case PlayerState.Shot:
                
                break;
            case PlayerState.Dash:
                body.isKinematic = false;
                projectile.HideProjectile();
                dashTween?.Kill();
                //body.velocity = curVel;
                break;
            case PlayerState.WallClimb:

                wallVelocity = false;
                if (WallSlideParticle.isPlaying == true)
                    WallSlideParticle.Stop();

                anim.SetBool("ClimbUp", false);
                anim.SetBool("Climb", false);

                if (newState != PlayerState.WallJump)
                {
                }

                if (newState != PlayerState.Aim)
                {   
                    body.isKinematic = false;
                }
                break;
        }

        switch (newState)
        {
            case PlayerState.Idle:
                anim.SetBool("Idle", true);
                break;
            case PlayerState.Walk:
                anim.SetBool("Walking", true);
                break;
            case PlayerState.Jump:
                if (currentState != PlayerState.Dash)
                    cutJump = true;
                break;
            case PlayerState.Aim:
                keyboard = Input.GetMouseButton(0);
                gunPivot.gameObject.SetActive(true);
                timeScaleTween?.Kill();
                timeScaleTween = DOTween.To(() => Time.timeScale, x => Time.timeScale = x, 0.05f, aimTimeScale).SetUpdate(true).OnUpdate(() =>
                {
                    Time.fixedDeltaTime = Time.timeScale * 0.02f;
                });
                break;
            case PlayerState.Shot:
                body.isKinematic = true;
                body.velocity = Vector3.zero;                
                projectile.Shot(shotForce, shotDirection, transform.localScale.x);
                break;
            case PlayerState.Dash:
                body.isKinematic = true;
                float time = Vector2.Distance(transform.position, projectile.transform.position) / speedDash;
                dashTween?.Kill();
                //dashTween = transform.DOMove(projectile.transform.position, time).SetEase(Ease.InSine).OnComplete(() => ChangeState(PlayerState.Idle));

                dashDirection = projectile.transform.position - transform.position;
                dashDirection.Normalize();
                curVel = Vector2.zero;

                dashTween = DOTween.To(() => curVel, x => curVel = x, dashDirection * speedDash, timeAccelartionDash).SetEase(Ease.InSine, 2);
                break;
            case PlayerState.WallClimb:
                if (currentState != PlayerState.Dash && body.velocity.y <= -15)
                {
                    wallVelocity = true;
                    currentWallTime = Time.time;

                }

                body.velocity = Vector3.zero;
                body.isKinematic = true;
                anim.SetBool("Climb", true);
                break;
            case PlayerState.WallJump:

                if (wallJumpTween != null)
                    StopCoroutine(wallJumpTween);

                wallJumpTween = StartCoroutine(WallJumpAir());
                break;
        }

        currentState = newState;
    }

    public bool wallVelocity;
    private Coroutine wallJumpTween;
    private float currentWallTime;
    public void WallState()
    {
        InputControllers();

        Vector2 direction = Vector2.zero;
        direction.y = axisY * wallSpeed;

        if(wallVelocity == true)
        {
            if(Time.time > currentWallTime +  0.8f)
            {
                wallVelocity = false;
            }
        }

        if (axisY < 0 || wallVelocity)
        {
            if (WallSlideParticle.isPlaying == false)
                WallSlideParticle.Play();

            direction.y = wallSlideSpeed;
        }else if (axisY > 0.3f)
        {
            if (WallSlideParticle.isPlaying == true)
                WallSlideParticle.Stop();
            anim.SetBool("ClimbUp", true);
        }
        else
        {
            if (WallSlideParticle.isPlaying == true)
                WallSlideParticle.Stop();
            anim.SetBool("ClimbUp", false);
        }

        if (onLeftWall)
        {
            ChangeSide(-1);
        }
        else if (onRightWall)
        {
            ChangeSide(1);
        }

        Vector3 origin      = transform.position + (Vector3.up * 0.5f);
        float side          = transform.localScale.x;
        RaycastHit2D hit    = Physics2D.Raycast(origin, transform.right * side, 1,1 << 8);

        Debug.DrawLine(origin, origin + (transform.right * (side * 1)), Color.blue);

        if (onHeadWall == false)
        {

            if(axisY > 0)
            {
                ChangeState(PlayerState.WallJump);
                return;
            }
            else
            {
                direction.y = wallSlideSpeed * 0.5f;
            }

            //direction.x += 2 * transform.localScale.x;
        }

        if (Input.GetButtonDown("Aim") && hasGun)
        {
            ChangeState(PlayerState.Aim);
        }

        if (inputJumpDown)
        {
            ChangeState(PlayerState.WallJump);
            return;
        }

        body.velocity = direction;

        if (onGround == true || onLeftWall == false && onRightWall == false)
            ChangeState(PlayerState.Idle);

    }

    IEnumerator WallJumpAir()
    {
        Vector2 direction = Vector2.zero;


        bool up = false;
        if(onHeadWall == false)
        {
            Debug.Log("Climbou");
            body.velocity = Vector2.zero;
            body.isKinematic = true;
            body.velocity = Vector2.zero;
            anim.SetBool("WallClimbJump",true);
            yield return new WaitForSeconds(0.2f);
            direction.y = 15;
            //direction.x = 5 * transform.localScale.x;
            up = true;
            cursor = 0;
        }
        else
        {

            direction.y = jumpForce;
            if (onLeftWall)
            {
                direction.x = 10;
                onLeftWall = false;
                cursor = 1;
                ChangeSide(1);
            }
            else if (onRightWall)
            {
                cursor = -1;
                ChangeSide(-1);
                direction.x = -10;
                onRightWall = false;
            }
        }

        body.velocity = direction;
        body.isKinematic = false;

        if (up)
        {
            yield return new WaitForSeconds(0.1f);
            direction.y = body.velocity.y;
            direction.x = 3 * transform.localScale.x;
            body.velocity = direction;
        }
        yield return new WaitForSeconds(0.1f);

        anim.SetBool("WallClimbJump", false);

        ChangeState(PlayerState.Jump);
    }

    public void DashState()
    {
        float dist = Vector2.Distance(transform.position, projectile.transform.position);
        body.velocity = curVel;

        //if (body.velocity.x < 0)
        //    ChangeSide(-1);
        //else
        //    ChangeSide(1);
        

        if(dist < 0.5f)
        {
            cutJump         = false;
            dashDirection = projectile.transform.position - transform.position;
            dashDirection.Normalize();

            Vector2 temp = body.velocity.normalized;
            cursor = transform.localScale.x;
            //cursor = temp.x;

            body.velocity = temp * speedDash;
            ChangeState(PlayerState.Jump);
            return;
        }

        if (onRoofWall)
        {
            ChangeState(PlayerState.Jump);
        }
        else if(onLeftWall && onHeadWall || onRightWall && onHeadWall)
                    ChangeState(PlayerState.WallClimb);
        //else if (onLeftWall || onRightWall)
        //{
        //    if(!onRoof)
        //        ChangeState(PlayerState.WallClimb);
        //}
        //else if (onGround)
        //{
        //    ChangeState(PlayerState.Idle);
        //}
    }

    public void ShotState()
    {
        
    }
    public void CancelShot()
    {
        body.isKinematic = false;

        if(onLeftWall || onRightWall)
        {
            if (onHeadWall)
            {
                ChangeState(PlayerState.WallClimb);
                return;
            }
        }
        ChangeState(PlayerState.Idle);
    }


    public bool lockAim;
    public bool keyboard;
    public void AimState()
    {
        InputControllers();

        //if (aimInput == false)
        //    ChangeState(PlayerState.Idle);

        Vector3 mousePos    = Input.mousePosition;

        Vector3 pos         = cam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, cam.transform.position.z * -1));
        shotDirection       = pos - transform.position;

        shotDirection.Normalize();

        //keyboard = Input.GetMouseButton(0);
        if (!keyboard) 
        {
            if (onLeftWall == false && onRightWall == false)
            {
                if (axisX > 0)
                    ChangeSide(1);
                else if (axisX < 0)
                    ChangeSide(-1);


                //if (axisX > -0.05f && axisX < 0.05f)
                //{
                //    axisX = 0;
                //}
                //if (axisY > -0.05f && axisY < 0.05f)
                //{
                //    axisY = 0;
                //}

                shotDirection.x = axisX;
                shotDirection.y = axisY;

                shotDirection.Normalize();
                shotDirection.x *= transform.localScale.x;
            }

            if (onLeftWall)
            {
                ChangeSide(-1);
                if (axisX < 0)
                {
                    axisX = 0;
                }

                shotDirection.x = axisX;
                shotDirection.y = axisY;

                shotDirection.Normalize();
                shotDirection.x *= transform.localScale.x;
            }
            else if (onRightWall)
            {
                ChangeSide(1);
                if (axisX > 0)
                {
                    axisX = 0;
                }

                shotDirection.x = axisX;
                shotDirection.y = axisY;

                shotDirection.Normalize();
                shotDirection.x *= transform.localScale.x;
            }
        }
        else
        {
            if (shotDirection.x > 0)
                ChangeSide(1);
            else if (shotDirection.x < 0)
                ChangeSide(-1);

            shotDirection.x *= transform.localScale.x;
        }


        float angle = Mathf.Atan2(shotDirection.y, shotDirection.x) * Mathf.Rad2Deg;

        float newAngle = Mathf.LerpAngle(gunPivot.transform.localEulerAngles.z, angle, 1);

        gunPivot.transform.localEulerAngles = Vector3.forward * newAngle;

        if (axisX == 0 && axisY == 0 && !keyboard)
        {
            gunVisual.enabled = false;
        }
        else
        {
            gunVisual.enabled = true;
        }

            if (Input.GetButtonUp("Aim"))
        {
            if (axisX == 0 && axisY == 0 && !keyboard)
            {
                CancelShot();
                return;
            }
            ChangeState(PlayerState.Shot);
        }
    }

    public void IdleState()
    {
        InputControllers();
        anim.SetFloat("Air", body.velocity.y);


        if (inputJumpDown)
        {
            Vector2 newVelocity = body.velocity;
            newVelocity.y       = jumpForce;
            body.velocity       = newVelocity;
        }

        if (!onGround)
        {
            ChangeState(PlayerState.Jump);
            return;
        }

        if (inputLeft || inputRight)
        {
            ChangeState(PlayerState.Walk);
            return;
        }

        if (aimInput && hasGun)
        {
            ChangeState(PlayerState.Aim);
            return;
        }
    }

    public void WalkState()
    {
        

        InputControllers();
        anim.SetFloat("Air", body.velocity.y);

        if (cursor < 0)
        {
            anim.speed = cursor * -1;
        }
        else
        {
            anim.speed = cursor;
        }

        if (inputLeft && body.velocity.x > 0 || inputRight && body.velocity.x < 0)
        {
            anim.SetBool("GroundSlide", true);
        }
        else
            anim.SetBool("GroundSlide", false);


        Vector2 newVelocity = body.velocity;
        float speedTarget   = cursor * walkSpeed;

        if (inputJumpDown)
        {
            newVelocity.y   = jumpForce;
        }



        //newVelocity.x = Mathf.Lerp(newVelocity.x, speedTarget, accelerationCurve.Evaluate(cursor));
        newVelocity.x = speedTarget;
        body.velocity = newVelocity;

        if (aimInput && hasGun)
        {
            ChangeState(PlayerState.Aim);
            return;
        }

        if (!onGround)
        {
            ChangeState(PlayerState.Jump);
            return;
        }

        if (newVelocity.x == 0)
        {
            ChangeState(PlayerState.Idle);
            return;
        }       
    }

    public void AirState()
    {
        InputControllers();

        anim.SetBool("Grounded", onGround);
        anim.SetFloat("Air", body.velocity.y);

        Vector2 newVelocity = body.velocity;
        float speedTarget   = cursor * walkSpeed;

        //newVelocity.x = Mathf.Lerp(newVelocity.x, speedTarget, accelerationAir.Evaluate(cursor));


        if (inputLeft && newVelocity.x > -walkSpeed)
            newVelocity.x -= Time.deltaTime * speedAirAcceleration * walkSpeed;
        if (inputRight && newVelocity.x < walkSpeed)
            newVelocity.x += Time.deltaTime * speedAirAcceleration * walkSpeed;

        //newVelocity.x = speedTarget;

        if (body.velocity.y < 5)
            cutJump = false;

        if(cutJump && !inputJumpUp)
        {
            cutJump = false;
            newVelocity.y = 5;
        }
        if (onRoof && newVelocity.y > 0)
            newVelocity.y = 0;

        newVelocity.y = Mathf.Clamp(newVelocity.y, -15, 100);


        body.velocity = newVelocity;

        if (onLeftWall || onRightWall)
        {
            if (!onRoof)
            {
                if (onHeadWall && body.velocity.y <=0)
                {
                    ChangeState(PlayerState.WallClimb);
                    return;
                }
            }            
        }            

        if (aimInput && hasGun)
        {
            ChangeState(PlayerState.Aim);
            return;
        }

        if (onGround)
            ChangeState(PlayerState.Walk);
    }

    public void InputControllers()
    {
        inputJumpDown   = Input.GetButtonDown("Jump");
        inputJumpUp     = Input.GetButton("Jump");
        axisX           = Input.GetAxisRaw("Horizontal");
        axisY           = Input.GetAxisRaw("Vertical");
        aimInput        = Input.GetButton("Aim");

        //Debug.Log("X: " + axisX + ", Y: " + axisY);

        inputRight  = axisX > 0 ? true : false;
        inputLeft   = axisX < 0 ? true : false;

        float curAcceleration = speedAcceleration;

        if (onGround == false)
            curAcceleration = speedAirAcceleration;

        if (inputRight)
        {
            cursor += Time.deltaTime * curAcceleration;
            ChangeSide(1);
            if (onRightWall)
                cursor = 0;
        }
        else if (inputLeft)
        {
            cursor -= Time.deltaTime * curAcceleration;
            ChangeSide(-1);
            if (onLeftWall)
                cursor = 0;
        }
        else if (onGround == true)
        {
            if(cursor > 0)
            {
                cursor -= Time.deltaTime * curAcceleration;                
            }
            else if(cursor < 0)
            {
                cursor += Time.deltaTime * curAcceleration;
            }
            if( cursor < 0.05f && cursor > -0.05f)
            {
                cursor = 0;
            }
        }

        #region Comentado
        /*
        if (inputRight)
        {
            if (body.velocity.x < 0)
            {
                cursor -= Time.deltaTime * speedAccelartion;
            }
            else
            {
                walkDirection = 1;
                cursor += Time.deltaTime * speedAccelartion;
            }
        }
        else if (inputLeft)
        {
            desaceleration  = 0;
            if (body.velocity.x > 0)
            {
                cursor -= Time.deltaTime * speedAccelartion;
            }
            else
            {
                walkDirection = -1;
                cursor += Time.deltaTime * speedAccelartion;
            }
        }
        else
        {
            cursor -= Time.deltaTime * speedAccelartion;
        }
        */
        #endregion

        cursor = Mathf.Clamp(cursor, -1, 1);
    }

    public void InputAir()
    {
        inputJumpDown   = Input.GetButtonDown("Jump");
        inputJumpUp     = Input.GetButtonUp("Jump");
        axisX           = Input.GetAxisRaw("Horizontal");
        aimInput        = Input.GetButton("Aim");

        inputRight  = axisX > 0 ? true : false;
        inputLeft   = axisX < 0 ? true : false;

        if (inputRight)
        {
            cursor += Time.deltaTime * speedAirAcceleration;
            ChangeSide(1);
        }
        else if (inputLeft)
        {
            cursor -= Time.deltaTime * speedAirAcceleration;
            ChangeSide(-1);
        }

        #region comentado
        /*
        if (inputRight)
        {
            if (body.velocity.x < 0)
            {
                walkDirection = 0;
                cursor -= Time.deltaTime * speedAcceleration;
            }
            else
            {
                walkDirection = 1;
                cursor += Time.deltaTime * speedAcceleration;
            }
        }
        else if (inputLeft)
        {
            if (body.velocity.x > 0)
            {
                walkDirection = 0;
                cursor -= Time.deltaTime * speedAcceleration;
            }
            else
            {
                walkDirection = -1;
                cursor += Time.deltaTime * speedAcceleration;
            }
        }
        */
        #endregion

        cursor = Mathf.Clamp(cursor, -1, 1);

        if (currentState != PlayerState.Aim && aimInput && hasGun)
        {
            ChangeState(PlayerState.Aim);
        }

    }

    public void ChangeSide(int side)
    {
        Vector3 newScale = Vector3.one;
        newScale.x = side;
        transform.localScale = newScale;
    }

    public void CheckWall()
    {
        Vector3 origin = transform.position + (Vector3.up * 0.5f);
        float side = transform.localScale.x;
        onHeadWall = Physics2D.Raycast(origin, transform.right * side, 1, 1 << 8);

        onRoof = Physics2D.OverlapCircle((Vector2)transform.position + roofOffset, groundRadius, 1 << 8);

        if (blockWall == false)
        {
            onLeftWall = Physics2D.OverlapCircle((Vector2)transform.position + leftOffset, wallRadius, 1 << 8);
            onRightWall = Physics2D.OverlapCircle((Vector2)transform.position + rightOffset, wallRadius, 1 << 8);
        }

        onGround = Physics2D.OverlapCircle((Vector2)transform.position + bottomOffset, groundRadius, 1 << 8);
    }
    public void Checkground()
    {
    }


    public void ResetPlayer(Transform checkpointPosition)
    {
        Vector2 newPos = checkpointPosition.position;
        newPos += Vector2.one;  
        transform.position = newPos;
        visual.gameObject.SetActive(true);
        ChangeState(PlayerState.Idle);
        axisX = 0;
        axisY = 0;
        dead = false;
        wallVelocity = false;
        body.velocity = Vector2.zero;
    }

    public void DeadPlayer()
    {
        dead = true;
        GameController.Instance.Death();
        visual.gameObject.SetActive(false);
        body.velocity = Vector2.zero;
        DeadParticle.Play();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (dead == false && collision.CompareTag("Kill"))
        {
            //ChangeState(PlayerState.Dead);
            DeadPlayer();
        }
        if (collision.CompareTag("Checkpoint"))
        {
            GameController.Instance.curCheckpoint = collision.transform;
        }
        if (collision.CompareTag("Gun"))
        {
            collision.gameObject.SetActive(false);
            hasGun = true;
        }
    }

    public bool dead = false;
    public bool blockWall;
    public float wallRadius = 0.25f;
    public float groundRadius = 0.25f;
    public Vector2 bottomOffset, rightOffset, leftOffset, roofOffset;
    

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        var positions = new Vector2[] { bottomOffset, rightOffset, leftOffset };

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere((Vector2)transform.position + bottomOffset, groundRadius);
        Gizmos.DrawWireSphere((Vector2)transform.position + rightOffset, wallRadius);
        Gizmos.DrawWireSphere((Vector2)transform.position + leftOffset, wallRadius);
        Gizmos.DrawWireSphere((Vector2)transform.position + roofOffset, groundRadius);
    }

    
}
