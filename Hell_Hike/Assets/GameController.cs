﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : Singleton<GameController>
{
    public delegate void GameEvent();
    public GameEvent DoCheckpoint;
    public GameEvent Death;

    public Transform curCheckpoint;

    public PlayerController player;

    public void Start()
    {
        Death += PlayerDead;
        DoCheckpoint += Checkpoint;
    }

    public void PlayerDead()
    {
        StartCoroutine(DelayToCheckpoint());
    }

    public IEnumerator DelayToCheckpoint()
    {
        yield return new WaitForSeconds(1f);
        DoCheckpoint();
    }
    public void Checkpoint()
    {
        //player.transform.position = curCheckpoint.transform.position;
        player.ResetPlayer(curCheckpoint);
    }
}
