﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationParticles : MonoBehaviour
{
    public ParticleSystem particle;
    public void PlayParticle()
    {
        particle.Play();
    }
}
