﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Projectile : MonoBehaviour
{
    public LineRenderer line;
    public ParticleSystem particle;
    private SpriteRenderer visual;

    Rigidbody2D body;

    public PlayerController owner;

    public bool flying;
    public bool drawning;

    public bool retract;

    public float maxDistance = 10;
    public float speedRetract;

    private Vector2 groundPosition;

    private Tweener retractTween;

    public LayerMask layer;

    public void Start()
    {
        body = GetComponent<Rigidbody2D>();
        visual = GetComponent<SpriteRenderer>();
        HideProjectile();
        particle.transform.parent = null;
        particle.transform.localScale = Vector3.one;
        //line.transform.parent = null;
    }

    public void Update()
    {
        if (drawning)
        {
            //transform.position = groundPosition;
            line.SetPosition(0, Vector2.zero);
            float dist = Vector2.Distance(transform.position, owner.transform.position);
            line.SetPosition(1, Vector2.right * dist); 
        }
        if (flying)
        {
            CheckGround();
        }
        
    }

    public void Shot(float Force, Vector3 direction, float side)
    {
        transform.position = owner.transform.position;

        line.SetPosition(0, Vector2.zero);
        line.SetPosition(1, Vector2.zero);

        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        line.transform.localEulerAngles = Vector3.forward * angle;

        direction.x *= side;
        angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.localEulerAngles      = Vector3.forward * angle;

        line.enabled    = true;
        visual.enabled  = true;
        flying          = true;
        drawning        = true;
        retract         = false;

        body.velocity = Force * direction;
    }

    [Header("TEst")]
    public float desaccelartionTime;
    public float holdTime;
    public float delayToRetract = 0.5f;
    public float easyValue;
    public void Retract()
    {
        //body.velocity = Vector3.zero;
        //HideProjectile();
        //owner.CancelShot();
        //flying = false;
        retract = true;
        retractTween?.Kill();
        retractTween = DOTween.To(() => body.velocity, x => body.velocity = x, Vector2.zero, desaccelartionTime).SetEase(Ease.OutSine, easyValue).OnComplete(() => 
        {
            float time = Vector2.Distance(transform.position, owner.transform.position) / speedRetract;
            retractTween = transform.DOMove(owner.transform.position, time).SetEase(Ease.InSine).SetDelay(delayToRetract).OnComplete(() =>
            {
                HideProjectile();
                owner.CancelShot();
            });
        });


    }

    IEnumerator Grab()
    {
        retractTween?.Kill();
        flying = false;
        body.velocity = Vector3.zero;

        yield return new WaitForSeconds(holdTime);
        owner.ChangeState(PlayerState.Dash);               
    }
    

    public void CheckGround()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.right, 0.5f, layer);
        Debug.DrawLine(transform.position, transform.position + (transform.right * 0.5f), Color.red);
        float dist = Vector2.Distance(transform.position, owner.transform.position);
        
        if (hit)
        {
            particle.transform.position = hit.point;
            float angle = Mathf.Atan2(hit.normal.y, hit.normal.x) * Mathf.Rad2Deg;
            particle.transform.localEulerAngles = Vector3.forward * angle;
            particle.Play();

            StartCoroutine(Grab());

            //flying = false;
            //body.velocity = Vector3.zero;
            //groundPosition = hit.point;
            //owner.ChangeState(PlayerState.Dash);
            //return;
        }
        else if(retract == false && dist > maxDistance)
        {
            Retract();
        }
    }

    public void HideProjectile()
    {
        line.enabled = false;
        visual.enabled = false;
        drawning = false;
        flying = false;
        retract = false;
    }
}
