﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterTrigger : MonoBehaviour
{
    Rigidbody2D body;

    public float force;
    public float negativeForce;

    public ParticleSystem particles;

    public bool hasPlayer = false;

    public void Start()
    {
        body = GetComponent<Rigidbody2D>();
        negativeForce = -force;
    }

    public void OnValidate()
    {
        negativeForce = -force;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (!hasPlayer && collision.CompareTag("Player"))
        {
            hasPlayer = true;
            //float curForce = force;
            //if (collision.transform.position.y > transform.position.y)
            //{
            //    Debug.Log("negativou");
            //    curForce = negativeForce;
            //}
            PlayerController player = collision.GetComponent<PlayerController>();
            if (player.body.velocity.y <= -10)
                particles.Play();

            //if (player.body.velocity.y > -5 && player.body.velocity.y < 5)
            //    curForce = force * 0.1f;

            //body.AddForce(Vector2.up * curForce, ForceMode2D.Impulse);

            if(player.body.velocity.y > -0.1f && player.body.velocity.y < 0.1f)
                body.AddForce(Vector2.up * force, ForceMode2D.Impulse);
            else if(player.body.velocity.y < -1)
                body.AddForce(Vector2.up * player.body.velocity.y, ForceMode2D.Impulse);
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (hasPlayer && collision.CompareTag("Player"))
        {
            hasPlayer = false;

            //if (collision.transform.position.y > transform.position.y)
            //{                

                PlayerController player = collision.GetComponent<PlayerController>();
            //    if (player.body.velocity.y > 10)
            //    {
            //        body.AddForce(Vector2.up * force, ForceMode2D.Impulse);
            //        particles.Play();

            //    }else if(player.body.velocity.y < 0)
            //    {
            //    }
            //}
            if (player.body.velocity.y > -0.1f && player.body.velocity.y < 0.1f)
            {
                return;
            }
            else if (player.body.velocity.y > 1)
            {
                body.AddForce(Vector2.up * player.body.velocity.y * 0.2f, ForceMode2D.Impulse);
                particles.Play();

            }
        }
    }

}
