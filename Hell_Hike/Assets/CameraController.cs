﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Cinemachine;

public class CameraController : MonoBehaviour
{
    //[Header("Components")]
    //public PlayerController target;

    //[Header("Distance Values")]
    //public float minDistX;
    //public float maxDistX;
    //public float minDistY;
    //public float maxDistY;

    //[Header("Offset Values")]
    //public float offsetX;
    //public float offsetY;
    //public float offsetZ;

    //[Header("Smooth Values")]
    //public float smoothX;
    //public float smoothY;
    //public float smoothZ;

    //private Vector3 desiredPostion;

    CinemachineVirtualCamera cine;

    void Start()
    {
        //desiredPostion = target.transform.position;
        //desiredPostion.x += offsetX;
        //desiredPostion.y += offsetY;
        //desiredPostion.z += offsetZ;
        //transform.position = desiredPostion;

        cine = GetComponent<CinemachineVirtualCamera>();

        GameController.Instance.DoCheckpoint += Checkpoint;
        GameController.Instance.Death += PlayerDead;
    }

    public void PlayerDead()
    {
        cine.enabled = false;
    }
    public void Checkpoint()
    {
        cine.enabled = true;
    }

    // Update is called once per frame
    //void FixedUpdate()
    //{
    //    if(target != null)
    //    {
    //        Vector3 newPosition = transform.position;

    //        float distX = Mathf.RoundToInt((target.transform.position.x + offsetX) - (transform.position.x));
    //        float distY = Mathf.RoundToInt((target.transform.position.y + offsetY) - (transform.position.y));

    //        //Debug.Log("distX: " + distX + ", DistY: " + distY);

    //        if(distX > maxDistX || distX < minDistX)
    //        {
    //            desiredPostion.x = target.transform.position.x;
    //            desiredPostion.x += offsetX;
    //        }

    //        if(distY > maxDistY || distY < minDistY)
    //        {
    //            desiredPostion.y = target.transform.position.y;
    //            desiredPostion.y += offsetY;
    //        }

    //        desiredPostion.z = target.transform.position.z;
    //        desiredPostion.z += offsetZ;

    //        //newPosition = Vector3.Lerp(newPosition, desiredPos, smooth * Time.fixedDeltaTime);
    //        newPosition.x = Mathf.Lerp(newPosition.x, desiredPostion.x, smoothX);
    //        //if (target.grounded)
    //        newPosition.y = Mathf.Lerp(newPosition.y, desiredPostion.y, smoothY);
    //        newPosition.z = Mathf.Lerp(newPosition.z, desiredPostion.z, smoothZ);

    //        transform.position = newPosition;
    //    }

    //}
}

