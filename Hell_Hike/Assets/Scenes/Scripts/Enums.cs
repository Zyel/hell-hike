﻿public enum PlayerState
{
    Idle = 0,    
    Walk = 1,
    Jump = 2,
    Aim  = 3,
    Shot = 4,
    Dash = 5,
    WallClimb = 6,
    WallJump = 7,
    Dead = 8,
    None = 9
}